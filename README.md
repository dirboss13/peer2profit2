# Peer2Profit

SHARE YOUR TRAFFIC AND PROFIT ON IT!

## How to run it?

- Clone the repository
- Run

```bash
docker-compose up -d
```

- To run multi p2pclient client

```bash
docker-compose up -d --scale p2pclient=3
```
